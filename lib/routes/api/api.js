const express = require('express')
const router = express.Router()
const UserDB = require('../../database/users')
const InventoryDB = require('../../database/inventory')
const multer = require('multer')
const upload = multer({ dest: 'avatars/'})

module.exports = function(url, secret) {
    this.router = router
    this.userdb = new UserDB(url)

    this.router.route('/users').get( (req, res) => {
        let list = []
        let docs = this.userdb.model.find({}, null, (err, docs) => { 
            for (var i = 0; i < docs.length; i++) {
                list.push({
                    id: docs[i]._id,
                    username: docs[i].username,
                    email: docs[i].email,
                    avatar: docs[i].avatar,
                    fullName: docs[i].fullName,
                    isAdmin: docs[i].isAdmin,
                    permissions: docs[i].permissions,
                    createdOn: docs[i].createdOn
                })
            }
            console.log(list)
            res.json(list)
        })
        
    }).post( (req, res) => {
        if(req.query.secret === secret) {
            if(req.query.username == null || 
            req.query.password == null ||
            req.query.email == null ||
            req.query.avatar == null ||
            req.query.fullName == null ||
            req.query.isAdmin == null ||
            req.query.permissions == null) {
                res.send('a field is missing')
            } else {
                this.userdb.createUser(
                    req.query.username, 
                    req.query.password, 
                    req.query.email,
                    req.query.avatar,
                    req.query.fullName,
                    req.query.isAdmin,
                    req.query.permissions
                    )
                res.send('User has been created')
                console.log('User has been created')
            }
            
        } else {
            res.send("Try again")
            console.log(`WARNING: IP ${req.ip} tried creating a user but failed to provide the secret key`)
        }
    }).put( (req, res) => {
        if(req.query.secret === secret) {
            this.userdb.updateUser(req.query.username, req.query.property, req.query.update, (status) => {
                if(status === "success") {
                    res.send(`${req.query.property} update successful on ${req.query.username}`)
                    console.log(`${req.query.property} update successful on ${req.query.username}`)
                } else {
                    res.send('fail')
                    console.log('Failed to update user information')
                }
            })
        } else {
            res.send('Try again')
            console.log(`WARNING: IP ${res.ip} tried to modify a user but failed to provide the secret key`)
        }
    })

    // this.router.route('/user/:username/avatar').get( (req, res) => {
    //     const options = {
    //         root: __dirname + '/avatars/',
    //         dotfiles: 'deny',
    //         headers: {
    //             'x-timestamp': Date.now(),
    //             'x-sent': true
    //         }
    //     }
    //     let user = this.userdb.model.findOne({username: res.query.username}, (err, doc) => {
    //         res.sendFile(doc.avatar, options, (err) => {
    //             if (err) console.error(err)
    //             else console.log('Sent: ', doc.avatar)
    //         })
    //     })
    // }).post(upload.single('avatar'), (req, res) => {

    // })

    this.router.get('/', (req, res) => {
        console.log('In api')
        res.send('lol')
    })
}