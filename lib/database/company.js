const mongoose = require('mongoose')
const Schema = mongoose.Schema

const locationsSchema = new Schema({
    location: { type: String }
})

const phoneSchema = new Schema({
    location: { type: String },
    number: { type: String }
})

const companySchema = new Schema({
    name: { type: String },
    email: { type: String },
    phone: [phoneSchema],
    locations: [locationsSchema],
})

module.exports = function(url) {
    let mongooseOptions = {
        autoIndex: false,
        reconnectTries: 30,
        reconnectInterval: 500,
        poolSize: 10,
        bufferMaxEntries: 0,
        useNewUrlParser: true
    }
    this.connect = function() {
        return mongoose.createConnection(url, mongooseOptions, (err) => {
            if (err) {
                console.error(`${err}\n\nThe url you provided was '${this.url}'`)
                process.exit()
            }
        })
    }
    this.connection = this.connect()
    this.model = this.connection.model('user', companySchema)
    this.newCompany = (company) => {
        if(this.model.findOne() != null) {
            let info = new this.model({
                name: company.name,
                email: company.email,
                phone: [
                    { location: company.location, number: company.number }
                ],
                locations: [
                    { location: company.location }
                ]
            })
            info.save((err) => { return err })
        }
    }
}