const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcrypt')
const salt = 10

const userSchema = new Schema({
    username: { type: String, required: true },
    password: { type: String, required: true },
    email: { type: String, required: true },
    phoneNumber: { type: String },
    avatar: { type: String },
    fullName: { type: String, required: true },
    isAdmin: { type: Boolean, required: true },
    permissions: { type: String, required: true },
    createdOn: { type: Date, default: Date.now() }
})

module.exports = function(url, root) {
    let mongooseOptions = {
        autoIndex: false,
        reconnectTries: 30,
        reconnectInterval: 500,
        poolSize: 10,
        bufferMaxEntries: 0,
        useNewUrlParser: true
    }
    this.connect = function() {
        return mongoose.createConnection(url, mongooseOptions, (err) => {
            if (err) {
                console.error(`${err}\n\nThe url you provided was '${url}'`)
                process.exit()
            }
        })
    }
    this.connection = this.connect()
    this.users = this.connection.model('user', userSchema)
    this.createUser = async function(username, password, email, avatar, fullName, isAdmin, permissions) {
        let user = await new this.users({
            username: username,
            password: await bcrypt.hash(password, salt),
            email: email,
            avatar: `${root}public/img/avatars/${avatar}`,
            fullName: fullName,
            isAdmin: isAdmin,
            permissions: permissions,
        })
        user.save()
    }
    this.findUser = function(username) {
        return this.users.findOne({username: username}, (err, user) => {
            if(err) console.error(err)
            else return user
        })
    }
    this.findUsers = function() {
        this.users.find({}, null, (err, docs) => {
            return docs
        })
    }
    this.removeUser = (username)=>{
        this.users.deleteOne({username: username})
    }
    this.updateUser = (username, prop, update) => {
        try{
            switch(prop) {
                case 'username':
                this.users.updateOne({username: username}, {username: update})
                break
                case 'password':
                this.users.updateOne({username: username}, {password: update})
                break
                case 'email':
                this.users.updateOne({username: username}, {email: update})
                break
                case 'avatar':
                this.users.updateOne({username: username}, {avatar: update})
                break
                case 'fullName':
                this.users.updateOne({username: username}, {username: update})
                break
                case 'isAdmin':
                this.users.updateOne({username: username}, {isAdmin: update})
                break
                case 'permissions':
                this.users.updateOne({username: username}, {permissions: update})
                break
            }

        } catch(err) {
            console.error(`There was a problem updating the user\n${err}`)
        }
    }
    this.verifyUser = async (username, password) => {
        try {
            let user = this.users.findOne({username: username})
            return {
                isValid: await bcrypt.compare(password, user.password),
                isAdmin: user.isAdmin,
                permissions: user.permissions
            }
        } catch(err) {
            console.error(err)
        }
        
    }
}