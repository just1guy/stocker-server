const mongoose = require('mongoose')
const Schema = mongoose.Schema
const ObjectId = Schema.Types.ObjectId

module.exports = function(url, root) {

    const locationsSchema = new Schema({
        location: { type: String },
        phones: [String],
        emails: [String],
    })
    
    const vendorsSchema = new Schema({
        name: { type: String },
        locations: [locationsSchema],
        url: { type: String }
    })
    
    const historySchema = new Schema({
        user: { type: String },
        quantityChange: { type: Number },
        price: { type: Number },
        changedOn: { type: Date, default: Date.now }
    })
    
    const itemSchema = new Schema({
        prodName: { type: String },
        brand: { type: String },
        category: { type: String },
        modelNumber: { type: String },
        quantity: { type: Number },
        required: { type: Number },
        type: { type: String },
        location: { type: String },
        storageLocation: { type: String },
        icon: { type: String, get: v => `${root}items/icons/${v}` },
        UPC: { type: String },
        vendors: [ObjectId],
        manual: { type: String, get: v => `${root}items/SDS/${v}` },
        SDS: { type: String , get: v => `${root}items/SDS/${v}`},
        specs: [String],
        notes: { type: String },
        history: [historySchema],
        createdOn: { type: Date, default: Date.now }
    })

    this.url = url
    let mongooseOptions = {
        autoIndex: false,
        reconnectTries: 30,
        reconnectInterval: 500,
        poolSize: 10,
        bufferMaxEntries: 0,
        useNewUrlParser: true
    }
    this.connect = function() {
        return mongoose.createConnection(this.url, mongooseOptions, (err) => {
            if (err) {
                console.error(`${err}\n\nThe url you provided was '${this.url}'`)
                process.exit()
            }
        })
    }
    this.connection = this.connect()
    this.items = this.connection.model('items', itemSchema)

    this.newItem = (item, vendorId) => {
        
        let newItem = new this.items({
            prodName: item.prodName,
            brand: item.brand,
            category: item.category,
            modelNumber: item.model,
            quantity: item.quantity,
            required: item.required,
            type: item.type,
            location: item.location,
            storageLocation: item.storageLocation,
            icon: item.icon,
            UPC: item.UPC,
            vendors: vendorId,
            manual: item.manual,
            SDS: item.SDS,
            specs: item.specs,
            notes: item.notes,
            history: [
                {
                    user: item.user,
                    quantityChange: item.quantity,
                    price: item.price
                }
            ]
        })
        newItem.save()
    }

    this.updateItem = (id, field, value, vendorId, locationId) => {
        switch(field) {
            case 'prodName':
            this.model.updateOne({ _id: id }, { prodName: value })
            break
            case 'brand':
            this.model.updateOne({ _id: id }, { brand: value })
            break
            case 'category':
            this.model.updateOne({ _id: id }, { category: value })
            break
            case 'modelNumber':
            this.model.updateOne({ _id: id }, { modelNumber: value })
            break
            case 'quantity':
            this.model.updateOne({ _id: id }, { quantity: value })
            break
            case 'required':
            this.model.updateOne({ _id: id }, { required: value })
            break
            case 'type':
            this.model.updateOne({ _id: id }, { type: value })
            break
            case 'location':
            this.model.updateOne({ _id: id }, { location: value })
            break
            case 'storageLocation':
            this.model.updateOne({ _id: id }, { storageLocation: value })
            break
            case 'icon':
            this.model.updateOne({ _id: id }, { icon: value })
            break
            case 'UPC':
            this.model.updateOne({ _id: id }, { UPC: value })
            break
            case 'manual':
            this.model.updateOne({ _id: id }, { manual: value })
            break
            case 'specs':
            this.model.updateOne({ _id: id }, { specs: value })
            break
            case 'notes':
            this.model.updateOne({ _id: id }, { notes: value })
            break
        }
    }
}