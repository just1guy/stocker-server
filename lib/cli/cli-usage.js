const chalk = require('chalk')
const cliHeader = require('./cli-header')
const cliEnv = require('./cli-env')

module.exports = [
    {
        header: chalk.blue(cliHeader),
        raw: true,
        content: 'A cross-platform inventory management server\n\nUsage: `node app.js <command> [options ...]`'
    },
    {
        header: 'Options',
        optionList: optionDefinitions
    },
    {
        header: 'Environment Variables',
        content: cliEnv
    },
    {
        content: 'Project home: {underline https://gitlab.com/just1guy/stocker-server}'
    }
]