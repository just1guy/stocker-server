module.exports = [
    { name: 'verbose', alias: 'v', type: Boolean, description: "Display all logs while server runs" },
    { name: 'port', alias: 'p', type: Number, description: "Set web interface to specific port" },
    { name: 'apiport', alias: 'a', type: Number, description: "Set API to specific port" },
    { name: 'mongourl', alias: 'm', type: String, description: "Set to specific Mongo IP:Port\n(e.g. 127.0.0.1:27017)" },
    { name: 'rooturl', alias: 'r', type: String, description: "Set root URL of application\n(e.g. http://stocker-server.com)"},
    { name: 'help', alias: 'h', type: Boolean, description: "Display this help message" },
    { name: 'noclient', alias: 'n', type: Boolean, description: 'Disable web view interface \n(only desktop and mobile apps can connect)'},
    { name: 'https', alias: 's', type: Boolean, description: 'Enable HTTPS. Must include cert and key file.\nThis setting is not necessary if using a reverse proxy.'},
    { name: 'key', alias: 'k', type: String, description: 'Path to SSL key. Must have https enabled for this to work.' },
    { name: 'cert', alias: 'c', type: String, description: 'Path to SSL cert. Must have https enabled for this to work.' },
    { name: 'company', alias: 'b', type: String, description: 'Company name. A customization option.' },
]