const chalk = require('chalk')

module.exports = [
    {
        desc: "{bold PORT}",
        example: "The port used for Stocker web interface server"
    },
    {
        desc: "{bold API_PORT}",
        example: "The port used for Stocker API server"
    },
    {
        desc: "{bold MONGO_URL}",
        example: "The Mongo URL for your MongoDB\n(e.g. http://127.0.0.1:27017/stocker)"
    },
    {
        desc: "{bold STOCKER_KEY}",
        example: "The SSL key used for https\nThis is not necessary if running a reverse proxy."
    },
    {
        desc: "{bold STOCKER_CERT}",
        example: "The SSL cert used for https\nThis is not necessary if running a reverse proxy."
    },
    {
        desc: "{bold STOCKER_HTTPS}",
        example: "Tells Stocker whether https is enabled or not\nENABLED: (STOCKER_HTTPS=https)\nDISABLED: (STOCKER_HTTPS=http) or undefined"
    },
    {
        desc: "{bold ROOT_URL}",
        example: "The root URL that Stocker Server will use.\nDEFAULT: http://127.0.0.1:{PORT}/"
    },
    {
        desc: "{bold COMPANY}",
        example: "Used to customize the title of the web interface."
    },
]
