module.exports = function(options) {
    this.get = {
        port: function() { //         Get Port
            if(options.port == null) { 
                return process.env.PORT || 3000
            } else { 
                return options.port 
            } 
        },
        apiPort: function() { //      Get API Port
            if(options.port == null) {
                return process.env.API_PORT || 3001
            } else {
                return options.apiport
            }
        },
        mongoUrl: function() { //     Get MongoURL
            if(options.mongourl == null) { 
                return process.env.MONGO_URL || 'mongodb://127.0.0.1:27017/stocker' 
            } else {
                return options.mongourl
            }
        },
        http: function() { //         Get HTTP or HTTPS
            if(options.https == null) return process.env.STOCKER_HTTPS || 'http'
            else return options.https
        },
        rootUrl: function() { //      Get Root URL
            if (options.rooturl == null) {
                return process.env.ROOT_URL || `http://127.0.0.1/`
            } else return options.rooturl
        },
        company: function() { //      Get Company Name
            if(options.company == null) {
                return process.env.COMPANY || 'Stocker Server'
            } else return options.company
        },
        key: function() { //          Get HTTPS Key
            if(options.key == null) {
                return process.env.STOCKER_KEY || null
            } else return options.key
        },
        cert: function() { //         Get HTTPS Cert
            if(options.key == null) {
                return process.env.STOCKER_CERT || null
            } else return options.cert
        },
        host: function() {
            try {
                if(options.rooturl == null) {
                    if(process.env.ROOT_URL == null) {
                        return '127.0.0.1'
                    } else {
                        if(process.env.ROOT_URL.endsWith('/')) {
                            return process.env.ROOT_URL.match(/\/[a-z0-9.]{3,}\//)[0].match(/[a-z0-9.]{1,}/)[0]
                        }
                        else return process.env.ROOT_URL.match(/\/[a-z0-9.]{3,}/)[0].match(/[a-z0-9.]{1,}/)[0]
                    }
                } else {
                    if(options.rooturl.endsWith('/')) {
                        return options.rooturl.match(/\/[a-z0-9.]{3,}\//)[0].match(/[a-z0-9.]{1,}/)[0]
                    } else {
                        return options.rooturl.match(/\/[a-z0-9.]{3,}/)[0].match(/[a-z0-9.]{1,}/)[0]
                    }
                }
            } catch(err) {
                console.error('Hostname is unable to be parsed\nMake sure the root url contains "http(s)://"')
                process.exit(1)
            }
        },
        noClient: ()=>{
            if(options.noclient === false) {
                if(process.env.CLIENT_ENABLED.toLocaleLowerCase() == "true" ) {
                    return true
                } else return false
            } else return true
        }
    }
}