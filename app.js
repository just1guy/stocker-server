//      Imports
const CommandLineArgs = require('command-line-args')
const CommandLineUsage = require('command-line-usage')
const UserDB = require('./lib/database/users')
const InventoryDB = require('./lib/database/inventory')
const GeneratePassword = require('password-generator')
//      Command line arguments parser
const OptionDefinitions = require('./lib/cli/cli-option-defs')
const CliOptions = CommandLineArgs(OptionDefinitions)
//      PGP Import and setup
const openpgp = require('openpgp')
openpgp.initWorker({ path: 'openpgp.worker.js' })

if(CliOptions.help) {
    const usage = CommandLineUsage(require('./lib/cli/cli-usage'))
    console.log(usage)
} else {
    //      CLI Globals
    const cliGlobals = require('./lib/cli/cli-globals')
    const globals = new cliGlobals({
        port: CliOptions.port,
        apiport: CliOptions.apiport,
        mongourl: CliOptions.mongourl,
        https: CliOptions.https,
        rooturl: CliOptions.rooturl,
        company: CliOptions.company,
        key: CliOptions.key,
        cert: CliOptions.cert
    })
    //      Mongoose Objects
    const userdb = new UserDB(globals.get.mongoUrl(), globals.get.rootUrl())
    const inventorydb = new InventoryDB(globals.get.mongoUrl(), globals.get.rootUrl())

    ///////////////////////////////////////////////////////////////////////////////
    //      App begins here

    const start = async ()=>{
        //      Make PGP Key
        const pgpOptions = {
            userIds: [{ name: 'Stocker Server', email: 'robertmsale@zoho.com' }],
            curve: "ed25519",
            passphrase: await GeneratePassword(32, false)
        }
        console.log('Generating PGP key')
        const pgpkey = await openpgp.generateKey(pgpOptions).then((key)=>{
            return { privkey: key.privateKeyArmored, pubkey: key.publicKeyArmored, revcert: key.revocationCertificate }
        })

        //      PGP {de,en}crypt function
        const PGP = {
            encrypt: async (message, pubkey) => {
                let options = {
                    message: await openpgp.message.fromText(message),
                    publicKeys: (await openpgp.key.readArmored(pubkey)).keys
                }
                return await openpgp.encrypt(options).then(cyphertext=> {
                    return cyphertext.data
                })
            },
            decrypt: async (encrypted) => {
                const privKeyObj = (await openpgp.key.readArmored(pgpkey.privkey)).keys[0]
                privKeyObj.decrypt(pgpOptions.passphrase)
                let options = {
                    message: await openpgp.message.readArmored(encrypted),
                    privateKeys: [privKeyObj]
                }
                return await openpgp.decrypt(options).then(plaintext => {
                    return plaintext.data
                })
            }
        }

        const authenticate = async (data) => {  //      Authentication (comma delimited)
            let cleartext = await PGP.decrypt(data)
            let username = cleartext.split(',')[0]
            let password = cleartext.split(',')[1]
            let verify = await userdb.verifyUser(username, password).isValid
            if(verify.isValid) {
                return { isValid: true, isAdmin: verify.isAdmin, permissions: verify.permissions }
            } else return false
        }

        console.log(await PGP.encrypt('Testing', pgpkey.pubkey))
        console.log(await PGP.decrypt( await PGP.encrypt('Testing', pgpkey.pubkey) ))

        const server = require('http').createServer()
        const io = require('socket.io')(server, {
            path: '/',
            serveClient: false,
            pingInterval: 10000,
            pingTimeout: 5000,
            cookie: false
        })

        //      Begin Socket callback hell
        io.on('connection', (socket)=>{ //  as soon as they connect
            socket.emit('auth-urself-pls', pgpkey.pubkey) // Ask client to authenticate
            socket.on('authenticate', async (encrypted)=> { // get response from previous event
                let decrypted = await authenticate(await PGP.decrypt(encrypted))
                if(decrypted.isValid) { //   isValid
                    if(decrypted.isAdmin) { //   is an admin
                        console.log('New connection is Admin')
                    } else { //     Is not an admin
                        console.log('New connection is NOT Admin')
                    }
                } else {
                    console.log('New connection is unauthenticated')
                    socket.emit('Not Valid', 'Please gimme a valid user k thx')
                }
            })
        })
        server.listen(globals.get.apiPort())
        console.log('Server started on port ', globals.get.apiPort())
    }
    start()
}
    //      App ends here
    ///////////////////////////////////////////////////////////////////////////////

